<?php

namespace Database\Factories;

use App\Models\Car;
use App\Models\Rent;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class RentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Rent::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $statuses = collect([
            'request_rent',
            'request_accepted', //nunggu di pake / diambil
            'rented', //udah diambil lagi dipake
            'done',
            'request_rejected',
        ]);

        $car = Car::all()->random();
        $days = $this->faker->numberBetween(1, 7);

        return [
            //
            'user_id' => User::all()->random()->id,
            'car_id' => $car->id,
            'date'  => $this->faker->date(),
            'days'  => $days,
            'status' => $statuses->random(),
            'price' => $car->price,
            'amount' => $car->price * $days,
            'remarks' => $this->faker->paragraph()
        ];
    }
}
