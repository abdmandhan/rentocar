<?php

namespace Database\Factories;

use App\Models\Car;
use App\Models\CarBrand;
use App\Models\CarType;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Car::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name'  => 'CAR' . strtoupper($this->faker->bothify('???-###')),
            'unit'  => $this->faker->numberBetween(4, 20),
            'price' => $this->faker->numberBetween(300, 1000) * 1000,
            'photo' => $this->faker->imageUrl(),
            'plat_no'   => strtoupper($this->faker->bothify('? #### ???')),
            'seat'  => $this->faker->numberBetween(2, 6),
            'matic' => $this->faker->numberBetween(0, 1),
            'car_type_id'   => CarType::all()->random()->id,
            'car_brand_id'  => CarBrand::all()->random()->id,
        ];
    }
}
