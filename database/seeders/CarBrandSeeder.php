<?php

namespace Database\Seeders;

use App\Models\CarBrand;
use Illuminate\Database\Seeder;

class CarBrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $brands = [
            ['name'  => 'BMW'],
            ['name'  => 'Mercedes'],
            ['name'  => 'Tesla'],
            ['name'  => 'Toyota'],
            ['name'  => 'Inova'],
            ['name'  => 'Suzuki'],
            ['name'  => 'Honda'],
        ];

        foreach ($brands as $key => $value) {
            CarBrand::create($value);
        }
    }
}
