<?php

namespace Database\Seeders;

use App\Models\CarType;
use Illuminate\Database\Seeder;

class CarTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $types = [
            ['name' => 'Sedan'],
            ['name' => 'Mini Bus'],
            ['name' => 'SUV'],
        ];

        foreach ($types as $key => $value) {
            CarType::create($value);
        }
    }
}
