<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\CarBrand;
use App\Models\CarType;
use App\Models\Rent;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $data = (object) $request->validate([
            'name'      => ['required'],
            'email'     => ['required', 'email', 'unique:users'],
            'password'  => ['required', 'confirmed'],
        ]);

        $user = User::create([
            'name' => $data->name,
            'email' => $data->email,
            'password' => Hash::make($data->password)
        ]);

        $token  = $user->createToken('CUSTOMER TOKEN');

        return response()->json([
            'status'    => true,
            'data'      => [
                'user'  => $user,
                'token' => $token->plainTextToken
            ],
            'message'   => 'Register Sucess'
        ]);
    }

    public function login(Request $request)
    {
        $data = (object)$request->validate([
            'email' => ['required', 'exists:users'],
            'password' => ['required']
        ]);

        $user = User::where('email', $data->email)->first();

        if (!$user) {
            return response()->json([
                'status' => false,
                'message' => 'Incorrect Data',
            ], 400);
        }

        $checkPassword = Hash::check($data->password, $user->password);

        if ($checkPassword) {
            $token  = $user->createToken('CUSTOMER TOKEN');

            return response()->json([
                'status' => true,
                'message' => 'Success Login',
                'data'  => $token->plainTextToken
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Incorrect Data',
            ], 400);
        }
    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();

        return response()->json(['status' => true]);
    }

    public function options()
    {
        return response()->json([
            'data' => [
                'car_type'  => CarType::all(['id', 'name']),
                'car_brand' => CarBrand::all(['id', 'name']),
            ]
        ]);
    }

    public function profile(Request $request)
    {
        return response()->json($request->user());
    }

    public function saveProfile(Request $request)
    {
        $data = $request->validate([
            'name'      => ['required'],
            'email'     => ['required', 'unique:users,email,' . $request->user()->id],
            'photo'     => ['file', 'mimes:png,jpg'],
            'sim'       => ['file', 'mimes:png,jpg'],
        ]);

        //save photo
        $photo = '/user/photo-user-' . $request->user()->id . $request->file('photo')->getClientOriginalName();
        $sim = '/user/sim-user-' . $request->user()->id . $request->file('photo')->getClientOriginalName();

        $request->file('photo')->storeAs('public', $photo);
        $request->file('sim')->storeAs('public', $sim);

        $request->user()->update([
            'name'  => $data['name'],
            'email' => $data['email'],
            'photo' => $photo,
            'sim'   => $sim,
        ]);

        return response()->json(User::find($request->user()->id));
    }

    public function car()
    {
        return response()->json(
            Car::all()
        );
    }

    public function showCar(Request $request)
    {
        return response()->json(
            Car::find($request->id)
        );
    }

    public function rent(Request $request)
    {
        $data = $request->validate([
            'car_id'    => ['required', 'exists:cars,id'],
            'date'      => ['required', 'date'],
            'days'      => ['required', 'numeric'],
        ]);

        $car = Car::find($data['car_id']);

        $data = [
            'user_id'   => $request->user()->id,
            'car_id'    => $data['car_id'],
            'date'      => $data['date'],
            'days'      => $data['days'],
            'status'    => 'request_rent',
            'price'     => $car->price,
            'amount'    => $car->price * $data['days']
        ];

        $rent = Rent::create($data);

        return response()->json($rent);
    }

    public function rentHistory(Request $request)
    {
        return response()->json(
            Rent::with(['user', 'user_rent'])->where('user_id', $request->user()->id)->get()
        );
    }
}
