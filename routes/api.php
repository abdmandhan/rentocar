<?php

use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('/register', [UserController::class, 'register']);
Route::post('/login', [UserController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::post('/logout', [UserController::class, 'logout']);
    Route::get('/options', [UserController::class, 'options']);

    Route::get('/profile', [UserController::class, 'profile']);
    Route::post('/profile', [UserController::class, 'saveProfile']);
    Route::get('/car', [UserController::class, 'car']);
    Route::get('/car/{id}', [UserController::class, 'showCar']);
    Route::post('/rent', [UserController::class, 'rent']);
    Route::get('/rent', [UserController::class, 'rentHistory']);
});



// Route::middleware('auth:sanctum')->group(function(){
//     Route::get('')
// });
